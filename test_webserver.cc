#include <iostream>
#include <thread>
#include <unistd.h>

#include "webserver.hh"

int main()
{
    unsigned short port;
    std::unique_ptr<pEp::Webserver> web(
            pEp::Webserver::probing_port_range(pEp::net::ip::address::from_string("127.0.0.1"),
                    8080, 8089, port, "htdocs")
        );
    if (!web) {
        std::cerr << "cannot start webserver, no ports free\n";
        return 1;
    }

    std::cout << "serving http://127.0.0.1:" << port << "/test.html\n";

    std::cout << "adding handler for http://127.0.0.1:" << port << "/handler/sample\n";
    web->add_url_handler("/handler/(?<name>\\w+)",
        [](boost::cmatch m, const pEp::Webserver::request& req)->pEp::Webserver::response {
            auto res = pEp::Webserver::response{pEp::http::status::ok, req.version()};

// https://www.boost.org/doc/libs/1_73_0/libs/beast/doc/html/beast/ref/boost__beast__http__response.html

            res.set(pEp::http::field::content_type, "text/json; charset=utf-8");
            res.keep_alive(req.keep_alive());

            res.body() = 
                "{"
                  "\"Herausgeber\": \"Xema\","
                  "\"Nummer\": \"1234-5678-9012-3456\","
                  "\"Deckung\": 2e+6,"
                  "\"Waehrung\": \"EURO\","
                  "\"Inhaber\":"
                  "{"
                    "\"Name\": \"Mustermann\","
                    "\"Vorname\": \"Max\","
                    "\"maennlich\": true,"
                    "\"Hobbys\": [\"Reiten\", \"Golfen\", \"Lesen\"],"
                    "\"Alter\": 42,"
                    "\"Kinder\": [],"
                    "\"Partner\": null"
                  "}"
                "}";

            res.prepare_payload();
            return res;
        }
    );

    web->run();
    std::cin.get();
    web->shutdown();

    exit(0); // avoid SIGABRT

    return 0;
}

