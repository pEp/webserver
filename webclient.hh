#pragma once

#include <string>
#include <stdexcept>

namespace pEp
{
    // to avoid infinite redirection loops
    enum { MaxRedirectionLevel = 23 };

    class HttpError : public std::runtime_error
    {
    public:
        HttpError(unsigned error_code, const std::string& error_message);
    };


    class Webclient
    {
    public:
        Webclient(const std::string& server_name, bool secure, unsigned port=0)
        : m_server_name{server_name}
        , m_secure{secure}
        , m_port{port==0 ? (secure?443:80) : port}
        {}
        
        std::string get(const std::string& url, int redirection_level = 0);
    
    private:
        const std::string& m_server_name;
        const bool m_secure;
        const unsigned m_port;
    };

} // end of namespace pEp
