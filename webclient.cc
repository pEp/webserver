#include "webclient.hh"
#include "url_parser.hh"

#include <string>
#include <stdexcept>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/version.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/ssl/error.hpp>
#include <boost/asio/ssl/stream.hpp>

namespace pEp
{

namespace beast = boost::beast;     // from <boost/beast.hpp>
namespace http = beast::http;       // from <boost/beast/http.hpp>
namespace net = boost::asio;        // from <boost/asio.hpp>
namespace ssl = net::ssl;           // from <boost/asio/ssl.hpp>
using tcp = net::ip::tcp;           // from <boost/asio/ip/tcp.hpp>


namespace
{

std::string redirect(boost::string_view url_string, int redirection_level)
{
    const pEp::URL url = pEp::parse_url(url_string.data(), url_string.data()+url_string.size());
    
    if(!url.username.empty() || !url.password.empty())
    {
        throw pEp::HttpError(901, "Redirect to URL with username:password is not supported.");
    }
    
    const bool secure = (
        url.schema=="https" ? true :
            (url.schema=="http" ? false :
                throw pEp::HttpError(902, "Unknown URL schema \"" + url.schema + "\"")
            )
        );
    
    pEp::Webclient client{url.host, secure, url.port};
    return client.get(url.path_and_query, redirection_level);
}


template<class Stream>
std::string fetch_and_close(Stream& stream, const std::string& server, const std::string& url, int redirection_level)
{
    // Set up an HTTP GET request message
    http::request<http::string_body> req{http::verb::get, url, 11};
    req.set(http::field::host, server);
    req.set(http::field::user_agent, "pEp::Webclient 0.1");
    
    // Send the HTTP request to the remote host
    http::write(stream, req);
    
    // This buffer is used for reading and must be persisted
    beast::flat_buffer buffer;
    
    // Declare a container to hold the response
    http::response<http::dynamic_body> res;
    
    // Receive the HTTP response
    http::read(stream, buffer, res);
    
    switch(res.result())
    {
        case http::status::ok :
        case http::status::no_content: return boost::beast::buffers_to_string(res.body().data());
        
        case http::status::moved_permanently:
        case http::status::found: // was "moved temporarily"
        case http::status::see_other:
        case http::status::temporary_redirect:
        case http::status::permanent_redirect:
            if(redirection_level < pEp::MaxRedirectionLevel)
            {
                return redirect(res["Location"], redirection_level + 1);
            } else {
                throw pEp::HttpError(900, "Too many redirections!");
            }
        
        default: // HTTP error or unknown/not implemented response code.
            throw pEp::HttpError(res.result_int(), boost::beast::buffers_to_string(res.body().data() ));
    }
}

} // end of anonymous namespace


HttpError::HttpError(unsigned error_code, const std::string& error_message)
: runtime_error("HTTP Error: " + std::to_string(error_code) + " " + error_message)
{}


// based on https://www.boost.org/doc/libs/1_75_0/libs/beast/doc/html/beast/quick_start/http_client.html
std::string Webclient::get(const std::string& url, int redirection_level)
{
    std::string ret;
    
    // The io_context is required for all I/O
    net::io_context ioc;
    
    // These objects perform our I/O
    tcp::resolver resolver(ioc);
    
    // Look up the domain name
    auto const results = resolver.resolve(m_server_name, std::to_string(m_port));
    
    if(m_secure)
    {
        // The SSL context is required, and holds certificates
        ssl::context ctx(ssl::context::tlsv12_client);
        
        // This holds the root certificate used for verification
        //load_root_certificates(ctx);
        
        // Verify the remote server's certificate
        ctx.set_verify_mode(ssl::verify_peer);
        
        beast::ssl_stream<beast::tcp_stream> stream(ioc, ctx);
        
        // Set SNI Hostname (many hosts need this to handshake successfully)
        if(! SSL_set_tlsext_host_name(stream.native_handle(), m_server_name.c_str()))
        {
            beast::error_code ec{static_cast<int>(::ERR_get_error()), net::error::get_ssl_category()};
            throw beast::system_error{ec};
        }
        
        // Make the connection on the IP address we get from a lookup
        beast::get_lowest_layer(stream).connect(results);
        
        // Perform the SSL handshake
        stream.handshake(ssl::stream_base::client);
        ret = fetch_and_close(stream, m_server_name, url, redirection_level);

        // Gracefully close the socket
        beast::error_code ec;
        stream.shutdown(ec);
        
        // not_connected happens sometimes
        // so don't bother reporting it.
        //
        if(ec && ec != beast::errc::not_connected)
            throw beast::system_error{ec};
        
    }else{
        // HTTP, without TLS
        beast::tcp_stream stream(ioc);
    
        // Make the connection on the IP address we get from a lookup
        stream.connect(results);
        ret = fetch_and_close(stream, m_server_name, url, redirection_level);

        // Gracefully close the socket
        beast::error_code ec;
        stream.socket().shutdown(tcp::socket::shutdown_both, ec);
        
        // not_connected happens sometimes
        // so don't bother reporting it.
        //
        if(ec && ec != beast::errc::not_connected)
            throw beast::system_error{ec};
    }
    
    return ret;
}


} // end of namespace pEp
