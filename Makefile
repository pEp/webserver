# this ifeq is not optional, because otherwise build.conf would always be built

# BUILD_CONF must be specified relative to the repo root or must be an absolute path
# BUILD_CONF defaults to ./build.conf
REPO_ROOT_REL:=$(dir $(lastword $(MAKEFILE_LIST)))
ifndef BUILD_CONF
    BUILD_CONF=$(REPO_ROOT_REL)/build.conf
    -include $(BUILD_CONF)
else
    BUILD_CONF_EFF=$(BUILD_CONF)
    ifeq ($(dir $(BUILD_CONF)),./)
        BUILD_CONF_EFF=$(REPO_ROOT_REL)/$(BUILD_CONF)
    endif
    ifeq ($(wildcard $(BUILD_CONF_EFF)),)
        $(info BUILD_CONF must be specified relative to the repo root or must be an absolute path)
        $(error file specified using BUILD_CONF ($(BUILD_CONF)) not found)
    endif
    include $(BUILD_CONF_EFF)
endif

PREFIX?=$(HOME)/local/
SYS_PREFIX?=/usr

BOOST_INCLUDE?=$(PREFIX)/include/
BOOST_LIB?=$(PREFIX)/lib/


AR?=ar
CC?=cc
CXX?=c++
CFLAGS+=-std=c11

CXXFLAGS+=-std=c++11
CXXFLAGS+=-I$(BOOST_INCLUDE) -I$(PREFIX)/include -I$(SYS_PREFIX)/include
LDFLAGS+=$(BOOST_LIB)/libboost_regex.a $(BOOST_LIB)/libboost_filesystem.a

ifeq ($(DEBUG),release)
	CFLAGS+=-O3 -DNDEBUG -fvisibility=hidden
	CXXFLAGS+=-O3 -DNDEBUG -fvisibility=hidden
else
	CFLAGS+=-O0 -g -fvisibility=hidden
	CXXFLAGS+=-O0 -g -fvisibility=hidden
endif

TARGET=libpEpwebserver.a

ALL_SOURCE=$(filter-out test_%.cc,$(wildcard *.cc))
DEPENDS=$(subst .cc,.d,$(ALL_SOURCE))
ALL_OBJECTS=$(subst .d,.o,$(DEPENDS))

TEST_SOURCE=$(wildcard test_*.cc)
TEST_OBJECTS=$(subst .cc,.o,$(TEST_SOURCE))
TESTS=$(subst .cc,,$(TEST_SOURCE))

all: $(TARGET)

%.d: %.cc
	@set -e; rm -f $@; \
	$(CC) -MM $(CPPFLAGS) $(CFLAGS) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$

# If only the goal 'clean' is given, do not generate and include the '%.d' files.
ifneq ($(MAKECMDGOALS),clean)
	-include $(DEPENDS)
endif

$(TARGET): $(ALL_OBJECTS)
	$(AR) -cr $@ $^

%.o: %.cc
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $^

test_%: test_%.o $(TARGET)
	$(CXX) $(LDFLAGS) $(TARGET) -o $@ $<

test: $(TESTS)
	for i in $(TESTS) ; do ./$$i ; done

.PHONY: clean uninstall install

install: $(TARGET)
	mkdir -p "$(DESTDIR)$(PREFIX)"/include/pEp
	cp -v *.hh "$(DESTDIR)$(PREFIX)"/include/pEp
	cp -v libpEpwebserver.a "$(DESTDIR)$(PREFIX)"/lib/

uninstall:
	for i in *.hh; do rm -vf "$(DESTDIR)$(PREFIX)/include/pEp/$$i" ; done

clean:
	rm -vf *.a *.o *.d *.d.* $(TARGET) $(TESTS)
