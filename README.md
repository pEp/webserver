# webserver

a simple multithreaded webserver

for usage see [this sample](https://gitea.pep.foundation/fdik/webserver/src/branch/master/test_webserver.cc)

## Build Configuration

The build configuration file is called `build.conf` or can be specified (relative to repo root) using
the env var `BUILD_CONF`.

Use the file `build.conf.example` as a template.

```bash
cp build.conf.example build.conf
```

Then, tweak it to your needs.

## Build Dependencies

The boost headers and static libs are required. By default they are searched for in $PREFIX/include and $PREFIX/lib
To change this behaviour there are env/`build.conf` variables `BOOST_INCLUDE` and `BOOST_LIB`.


## Make Targets

### Build
`make` - The default make target builds the static lib the webserver.

### Test
`make test` - Builds and executes all tests

### Install
`make install` - Installs the library into your $PREFIX   
`make uninstall` - Uninstalls the library from your $PREFIX

### Clean
`make clean`
